alias cmd='gnome-terminal'
alias h='history'
alias fdsk='sudo fdisk -l|grep sd'
alias mountexfat='sudo mount -t exfat /dev/sdb1 /media/science'
alias mountntfs='sudo mount -t ntfs /dev/sdb1 /media/science'
#docker
alias DOCK='cd Docker/docker-oceano/'
alias build='docker build -t jgrelet/perl .'
alias lacie='docker run -it --rm -v /media/science/LaCie:/data jgrelet/perl /bin/bash'
alias 2T='docker run -it --rm -v /media/science/2T\ JGrelet:/data jgrelet/perl /bin/bash'
alias run='docker run -it --rm -v /mnt/irdimago:/data --env CRUISE=PIRATA-FR27 jgrelet/perl /bin/bash'
alias inc='perl -le"print for @INC"'

# owncloud
alias tailown='tail -f /home/science/logs/owncloud.log'

# cruise FR28
alias DATA='cd /mnt/data/DONNEES/PIRATAFR32'
