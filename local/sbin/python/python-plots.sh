#!/bin/bash

shopt -s expand_aliases
export CRUISE=$1
export DRIVE=$2
. ${DRIVE}/${CRUISE}/local/etc/skel/.bashrc.${CRUISE}
export NC_DIR=netcdf
export PROF_DIR=plots/python
export SECT_DIR=coupes/python

CTD
# all profiles
$LOCAL/sbin/python/plots.py $NC_DIR/OS_${CRUISE}_CTD.nc -t CTD -p -k PRES TEMP PSAL DOX2 FLU2 -g -c k- b- r- m- g- -g -o $PROF_DIR

## section 10W
#plots.py $NC_DIR/OS_${CRUISE}_CTD.nc -t CTD -s --append 1N-10W_10S_10W -k PRES TEMP --xaxis LATITUDE -l 5 28 --yscale 0 250 250 2000 --xinterp 24 --yinterp 200 --clevels=30 --autoscale 0 30 -o $SECT_DIR
#plots.py $NC_DIR/OS_${CRUISE}_CTD.nc -t CTD -s --append 1N-10W_10S_10W -k PRES PSAL --xaxis LATITUDE -l 5 28 --yscale 0 250 250 2000 --xinterp 24 --yinterp 200 --clevels=20 --autoscale 33 37 -o $SECT_DIR
#plots.py $NC_DIR/OS_${CRUISE}_CTD.nc -t CTD -s --append 1N-10W_10S_10W -k PRES DENS --xaxis LATITUDE -l 5 28 --yscale 0 250 250 2000 --xinterp 24 --yinterp 200 --clevels=20 --autoscale 20 30 -o $SECT_DIR
#plots.py $NC_DIR/OS_${CRUISE}_CTD.nc -t CTD -s --append 1N-10W_10S_10W -k PRES DOX2 --xaxis LATITUDE -l 5 28 --yscale 0 250 250 2000 --xinterp 24 --yinterp 200 --clevels=20 --autoscale 0 250 -o $SECT_DIR
#plots.py $NC_DIR/OS_${CRUISE}_CTD.nc -t CTD -s --append 1N-10W_10S_10W -k PRES FLU2 --xaxis LATITUDE -l 5 28 --yscale 0 250 --xinterp 24 --yinterp 200 --clevels 20 --autoscale 0 2 -o $SECT_DIR
## point fixe 0-10W
#plots.py $NC_DIR/OS_${CRUISE}_CTD.nc -t CTD -s --append point-fixe_0-10W -k PRES TEMP --xaxis TIME -l 33 48 --yscale 0 200 --xinterp 20 --yinterp 20 --clevels=30 --autoscale 0 30 -o $SECT_DIR
#plots.py $NC_DIR/OS_${CRUISE}_CTD.nc -t CTD -s --append point-fixe_0-10W -k PRES PSAL --xaxis TIME -l 33 48 --yscale 0 200 --xinterp 20 --yinterp 20 --clevels=20 --autoscale 33 37 -o $SECT_DIR
#plots.py $NC_DIR/OS_${CRUISE}_CTD.nc -t CTD -s --append point-fixe_0-10W -k PRES DENS --xaxis TIME -l 33 48 --yscale 0 200 --xinterp 20 --yinterp 20 --clevels=20 --autoscale 20 30 -o $SECT_DIR
#plots.py $NC_DIR/OS_${CRUISE}_CTD.nc -t CTD -s --append point-fixe_0-10W -k PRES DOX2 --xaxis TIME -l 33 48 --yscale 0 200 --xinterp 20 --yinterp 20 --clevels=20 --autoscale 0 250 -o $SECT_DIR
#plots.py $NC_DIR/OS_${CRUISE}_CTD.nc -t CTD -s --append point-fixe_0-10W -k PRES FLU2 --xaxis TIME -l 33 48 --yscale 0 200 --xinterp 20 --yinterp 20 --clevels 20 --autoscale 0 2 -o $SECT_DIR

XBT
# all profiles
$LOCAL/sbin/python/plots.py $NC_DIR/OS_${CRUISE}_XBT.nc -t XBT -p -k DEPTH TEMP DENS SVEL -c k- b- k- g- -g -o plots

## sections
#plots.py $NC_DIR/OS_${CRUISE}_XBT.nc -t XBT -s --append CANARIES_1-30N-10W -k DEPTH TEMP --xaxis LATITUDE -l 2 17 --yscale 0 900 -o $SECT_DIR
#plots.py $NC_DIR/OS_${CRUISE}_XBT.nc -t XBT -s --append 10S-20S_10W -k DEPTH TEMP --xaxis LATITUDE -l 18 28 --yscale 0 900 -o $SECT_DIR
#plots.py $NC_DIR/OS_${CRUISE}_XBT.nc -t XBT -s --append 10S-10W_0-0 -k DEPTH TEMP --xaxis LATITUDE -l 29 30 --yscale 0 900 -o $SECT_DIR
#plots.py $NC_DIR/OS_${CRUISE}_XBT.nc -t XBT -s --append 0-10W_0_23W -k DEPTH TEMP --xaxis LONGITUDE -l 39 52 --yscale 0 900 -o $SECT_DIR

LADCP
# all profiles
$LOCAL/sbin/python/plots.py $NC_DIR/OS_${CRUISE}_ADCP.nc -t ADCP -p -k DEPTH EWCT NSCT -c k- r- b- -g -o $PROF_DIR

## sections
#plots.py $NC_DIR/OS_${CRUISE}_ADCP.nc -t ADCP -s --append 1N-10W_10S_10W -k DEPTH EWCT NSCT -l 5 28 --xaxis LATITUDE --yscale 0 250 250 2000 --xinterp 24 --yinterp 100 --clevels 30 --autoscale -150 150 -o $SECT_DIR
#plots.py $NC_DIR/OS_${CRUISE}_ADCP.nc -t ADCP -s --append point-fixe_0-10W -k DEPTH EWCT NSCT -l 33 48 --xaxis TIME --yscale 0 500 --xinterp 26 --yinterp 25 --clevels 30 --autoscale -150 150 -o $SECT_DIR
